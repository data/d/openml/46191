# OpenML dataset: Hospital

https://www.openml.org/d/46191

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Monthly patient count for products that are related to medical problems. 

From original source:
-----
Monthly patient count for products that are related to medical problems. There are 767 time series
that had a mean count of at least 10 and no zeros.
-----

Extracted from 'expsmooth' R package (.csv available on official website)

Preprocessing:

1 - Melted the dataset with indentifiers 'MPriceHospLOS2000_SKUCode', 'MPriceHospLOS2000_RootEntityCode', obtaining columns 'date' and 'value'.

2 - Standardize the date to the format %Y-%m-%d.

3 - Renamed columns 'MPriceHospLOS2000_SKUCode', 'MPriceHospLOS2000_RootEntityCode' to 'covariate_0' and 'covariate_1'.

4 - Created column 'id_series' from covariate_0' and 'covariate_1' with index from 0 to 766.

5 - Created column 'time_step' with increasing values of the time_step for the time series.

6 - Casted 'value' columns to int, and defined 'id_series', covariate_0' and 'covariate_1' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46191) of an [OpenML dataset](https://www.openml.org/d/46191). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46191/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46191/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46191/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

